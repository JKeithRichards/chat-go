package main

import (
	"gopkg.in/zeromq/goczmq.v4"
	"fmt"
	"log"
	"time"
	"bufio"
	"os"
	"encoding/json"
	"strings"
	"sync"
)

// TODO: join/leave channels
var listenSockets = make(map[string]*goczmq.Sock)
var (
	listenEP string
	mu       sync.Mutex
)

type clientConfig struct {
	ServerIP    string `json:"serverIP"`
	ListensPort string `json:"listenPort"`
	TalkPort    string `json:"talkPort"`
	MyName      string `json:"myName"`
}

// TODO: generalize this with server version
func loadClientConfiguration(file string) clientConfig {
	var config clientConfig
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		panic(err)
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

// Get Local keyboard input and send to server
func talkToServer(serverIP string, talkPort string, myName string) {
	talkEP := "tcp://" + serverIP + ":" + talkPort
	talkSocket, err := goczmq.NewDealer(talkEP)
	if err != nil {
		log.Fatal(err)
	}
	defer talkSocket.Destroy()
	log.Println("Talking to server at " + talkEP)

	scanner := bufio.NewScanner(os.Stdin)

	// Give talking socket time to start
	time.Sleep(500 * time.Millisecond)

	var channel string = "general"
	fmt.Print("[" + channel + "] Enter Text: ")
	for scanner.Scan() {
		toSay := scanner.Text()
		if len(toSay) != 0 {
			// Check if client wants to change channel it's talking on
			if strings.HasPrefix(toSay, "+") {
				newChannel := strings.TrimPrefix(toSay, "+")
				channel = newChannel
				print("You are now talking on channel '" + newChannel + "'\n")
				newlistenSocket := subscribeToChannel(newChannel)
				defer newlistenSocket.Destroy()
				mu.Lock()
				listenSockets[channel] = newlistenSocket
				mu.Unlock()
				//}
			} else if strings.HasPrefix(toSay, "-") {
				deleteChannel := strings.TrimPrefix(toSay, "-")
				mu.Lock()
				delete(listenSockets, deleteChannel)
				mu.Unlock()
				print("You are no longer listening to channel '" + deleteChannel + "'\n")
				if channel == deleteChannel {
					channel = "general"
				}
			} else if strings.HasPrefix(toSay, "?") {
				channelList := ""
				for channel, _ := range listenSockets {
					channelList += channel + ","
				}
				println("You are listening to the following channels: " + channelList)
			} else {
				sendMsg := channel + " " + myName + ": " + toSay
				err := talkSocket.SendFrame([]byte(sendMsg), goczmq.FlagNone)
				if err != nil {
					log.Fatal(err)
				}
			}
			fmt.Print("[" + channel + "] Enter Text: ")
		}
	}
	time.Sleep(500 * time.Millisecond)
}

func subscribeToChannel(channel string) *goczmq.Sock {
	listenSocket, err := goczmq.NewSub(listenEP, channel)
	if err != nil {
		println("Error trying to subscribe to channel '" + channel + "': ")
		panic(err)
	}
	listenSocket.Connect(listenEP)
	fmt.Println("Listening to server at " + listenEP + ", channel: " + channel)
	return listenSocket
}

// Listen to the server, print messages to screen
func listenServer(serverIP string, listenPort string) {
	listenEP = "tcp://" + serverIP + ":" + listenPort
	channel := "general"
	newlistenSocket := subscribeToChannel(channel)
	defer newlistenSocket.Destroy()
	listenSockets[channel] = newlistenSocket
	for {
		mu.Lock()
		for channel, socket := range listenSockets {
			receiveMsg, _, err := socket.RecvFrameNoWait()
			splitMsg := strings.Split(string(receiveMsg), " ")
			justText := ""
			for index := 1; index < len(splitMsg); index++ {
				justText += splitMsg[index] + " "
			}
			justText = strings.TrimSpace(justText)
			if err == nil {
				fmt.Println("\n" + string(justText))
				fmt.Print("[" + channel + "] Enter Text: ")
			}
		}
		mu.Unlock()
		time.Sleep(500 * time.Millisecond)
	}
}

func main() {
	config := loadClientConfiguration("client.json")
	var removeSpaces = strings.NewReplacer(" ", "_")
	myName := removeSpaces.Replace(config.MyName)
	go talkToServer(config.ServerIP, config.TalkPort, myName)
	go listenServer(config.ServerIP, config.ListensPort)

	for {
		time.Sleep(500 * time.Millisecond)
	}
}
