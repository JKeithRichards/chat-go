package main

import (
	"gopkg.in/zeromq/goczmq.v4"
	"fmt"
	"log"
	"time"
	"os"
	"encoding/json"
	"strings"
	"io"
)

// TODO: delete channels with no participants
// TODO: server command: list who's in a channel

type serverConfig struct {
	ListensPort string `json:"listenPort"`
	TalkPort    string `json:"talkPort"`
	Logfile     string `json:"logfile"`
}

func loadServerConfiguration(file string) serverConfig {
	var config serverConfig
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		panic(err)
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

func addChannel(newChannel string, allChannels [] string) []string {
	needsAdded := true
	for _, existingChannel := range allChannels {
		if existingChannel == newChannel {
			needsAdded = false
			break
		}
	}
	if needsAdded {
		evenMoreChannels := append(allChannels, newChannel)
		return evenMoreChannels
	}
	return allChannels
}

func main() {
	// Read configuration settings from file
	config := loadServerConfiguration("server.json")

	// Open log file
	logfile, _ := os.Create(config.Logfile)
	mw := io.MultiWriter(os.Stdout, logfile)
	log.SetOutput(mw)
	log.Println("Saving messages to " + config.Logfile)

	// Open socket to listen to clients on
	listenEP := "tcp://*:" + string(config.ListensPort)
	listenSocket, err := goczmq.NewRouter(listenEP)
	if err != nil {
		log.Println("Is there a server already using port '" + config.ListensPort + "'?")
		log.Print("Failed to open listening socket  ")
		log.Fatal(err)
	}
	defer listenSocket.Destroy()
	listenSocket.Bind(listenEP)
	poller, err := goczmq.NewPoller(listenSocket)
	if err != nil {
		log.Print("Failed start poller on listening socket: ")
		log.Fatal(err)
	}
	log.Println("Listening at " + listenEP)

	// Open socket to talk to clients
	talkEP := "tcp://127.0.0.1:" + config.TalkPort
	talkSocket := talkEP
	pubSock, err := goczmq.NewPub(talkSocket)
	if err != nil {
		log.Print("Failed to open talking socket: ")
		log.Fatal(err)
	}
	defer pubSock.Destroy()
	pubSock.Bind(talkSocket)
	log.Println("Talking at " + talkEP)

	// Listen for talk and repeat to all clients
	var allChannels []string
	for {
		socket := poller.Wait(500)
		if socket == listenSocket {
			msgParts, err := listenSocket.RecvMessage()
			if err != nil {
				log.Print("Error trying to receive message: ")
				log.Println(err)
			}
			now := time.Now()
			splitText := strings.Split(string(msgParts[1]), " ")
			justMsg := ""

			// Extract parts from incoming message
			currChannel := string(splitText[0])
			client := splitText[1]
			CommandText := splitText[2]
			allChannels = addChannel(currChannel, allChannels)

			// Check for server command in message
			if strings.HasPrefix(CommandText, "#") {
				var msg string
				switch CommandText {
				case "#list":
					channelList := "Available allChannels: "
					for index := range allChannels {
						channelList += string(allChannels[index]) + ","
					}
					channelList += "\n"
					log.Println("## " + client + " Requested list of all channels")
					time.Sleep(500 * time.Microsecond)
					msg = fmt.Sprintf("%s %s", currChannel, channelList)
				default:
					msg = "Unknown server command: " + CommandText + "\n"

				}
				log.Println(allChannels)
				err = pubSock.SendFrame([]byte(msg), 0)
				if err != nil {
					log.Print("Error trying to send message:")
					log.Println(err)
				}

			} else {
				//if splitText[1]
				for i := 2; i < len(splitText); i++ {
					justMsg += splitText[i] + " "
				}
				fullMsg := fmt.Sprintf(
					"(%s)[%s in '%s':] %s\n",
					now.Format("15:04:05"),
					client,
					currChannel,
					string(justMsg),
				)
				log.Print(fullMsg)
				msg := fmt.Sprintf("%s %s", currChannel, fullMsg)
				err = pubSock.SendFrame([]byte(msg), 0)
				if err != nil {
					log.Print("Error trying to send message:")
					log.Println(err)
				}
			}

		}
	}
}
