# Chat server and client
## Overview
This is code to run a chat server and clients. 

The code uses ZeroMQ for the message passing. Please see: http://zguide.zeromq.org/ 

For the Golang bindings it uses goczmq, 
for more information please see: https://github.com/zeromq/goczmq and https://godoc.org/gopkg.in/zeromq/goczmq.v4

The server opens a ZeroMQ Router socket to listen to clients. 
The client will open a Dealer socket to talk to the router. 
For more information please see http://zguide.zeromq.org/php:chapter3#The-DEALER-to-ROUTER-Combination

The server also open a Publish socket to talk to all connected clients. 
The clients will subscribe to the Publish socket to receive a copy of all messages from all clients.
For more information please see http://zguide.zeromq.org/php:chapter5


## Server
### Overview
As the Router socket name implies, the servers main job is to echo one clients message to all other clients. 

### Implementation Details
The Server creates a Router socket that all clients can connect to. 
It also creates a Publish socket to talk to the clients. 

The server then polls the Router socket waiting for messages from the clients. 
For all messages the Server receives, it first checks if the message is a server command. 
If so it acts accordingly. If not, it echos the message to all client. 

### Config file
In the file `server.json` you can set the ports the server should listen for clients on, and the port it should
talk to clients on. 

**TIP:** The servers listen socket should be the same port as the clients talk socket, and vice versa.

The servers log file name can also be specified here.  

### Server commands
Messages from the client starting with `#` are interpreted as commands to the the client.
For now there is only one:
* "#list" - List all channels that are being used

### Future Features
* The Server will controlling registration of clients to the server and to channels, 
for now we are trusting the clients to behave. 
* Verifying the config settings and hard coding sensible defaults
* New server command: "#who" - List all connected clients

## Client 
### Overview
The client accepts keyboard input and sends it to the Server. 
It also listens to messages from other other clients via the Server, and prints them to the console.

### Implementation Details
In a goroutine the client starts a "listen" socket connected to the servers "talk" socket.
Any channel a client wants to talk on or listen to, it must "subscribe" to the channel on the servers publish socket.
The client then loops through each subscribed socket listening for messages from the server. 

In another goroutine the client will start a "talk" socket connected to the servers "listen" socket.
Keyboard input is read and sent to the server through this socket. 

**Note**: The client does not echo the typed text to its own console. 
When the text appears on the console that means it has been sent to the server through the Router-Dealer
socket and than received through the Publisher-Subscriber socket. 
In other words, if you see the text on the console you know that all other clients in that channel are seeing it at well. 

### Config file
In the file `client.json` you can set the ports the client should listen to the server on, 
and the port it should talk to the server on. 

**TIP:** The client listen port should be the servers talk port, and vice versa.

### Channels
The server will put all client messages into the Publish socket, 
but ZeroMQ will only send the message to a particular client if it has subscribed to the channel the message is using. 
If a user tries to join a channel that doesn't exist, the channel is "created". 
Under the covers this means the Server adds the channel to its list of channels, 
and the client prepends all its messages with the channel.

### Client Commands
* "+" Start talking on and listening to a channel. eg: `+sports`
* "-" Stop listening to a channel. eg: `-sports`
* "?" List all channels currently being listened to, aka subscribed to. eg: `?`

### Future Features
* Rewrite `loadClientConfiguration` so that the Server and Client can use the same function.
* Redesign the code flow to remove/reduce the need for global variables.
* Clean up the console output to the user.
